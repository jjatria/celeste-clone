#!/usr/bin/env raku

use lib 'lib';

use Pop;
use Pop::Graphics;
use Pop::Entities;
use Local::Console;
use Celeste;

Pop.new: :fullscreen,
    title  => 'Celeste',
    width  => SCREEN-SIZE,
    height => SCREEN-SIZE;

load map     => 'resources/map',
     flags   => 'resources/flags',
     sprites => 'resources/sprites.png';

my     $SHAKE      = 0;
my Int $RESTART-IN = 0;

load-room( 7, 3 );

Pop.key-released: -> $_, $ {
    when 'ESCAPE' { Pop.stop }
    when so room.is-title {
        load-room( 0, 0 );
    }
}

Pop.update: {
    # Restart (soon)
    load-room( room.x, room.y ) if $RESTART-IN > 0 && --$RESTART-IN == 0;

    Pop::Entities.view(Particle).each: -> $_ {
        .xy     .= add: .speed, sin .offset;
        .offset += min( 0.05, .speed / 32 );
        .xy     .= new( -4, (^128).pick ) if .x > 129 + 4;
    }

    Pop::Entities.view(DeadParticle).each: -> $e, $_ {
        .xy .= add: .speed;
        Pop::Entities.delete: $e if --.t < 0;
    }

    unless room.is-title {
        Pop::Entities.view(Cloud).each: -> $_ {
            .xy .= add: .speed, 0;
            .xy .= new( -.size, (^120).pick ) if .x > 128;
        }
    }

    Pop::Entities.view(Body).each: -> $_ { .update }

    Pop::Entities.view(Spawn, Body, Drawable).each: -> $e, $s, $p, $d {
        given $s.state {
            when 0 { # jumping up
                if $p.y < $s.target.y + 16 {
                    $s.state = 1;
                    $s.delay = 3;
                }
            }
            when 1 { # falling
                $p.speed .= add: 0, 0.5;
                if $p.speed.y > 0 && $s.delay > 0 {
                    $p.speed .= mul: 1, 0;
                    $s.delay -= 1;
                }
                if $p.speed.y > 0 && $p.y > $s.target.y {
                    $p.xy .= new: $p.x, $s.target.y;
                    $p.speed .= zero;
                    $s.state = 2;
                    $s.delay = 5;
                    $SHAKE = 5;
                    # init_object(smoke,this.x,this.y+4) # TODO: Smoke
                    # sfx(5) # Sound
                }
            }
            when 2 { # landing
                $s.delay -= 1;
                $d.sprite = 6;
                if $s.delay < 0 {
                    given Pop::Entities {
                        .delete: $e, Spawn;
                        .add: $e, Player.new;
                        $d.sprite = 1;
                        $p.solid = True;
                    }
                }
            }
        }
    }

    my $input = Pop::Inputs.keyboard('LEFT')  ?? -1
             !! Pop::Inputs.keyboard('RIGHT') ??  1
             !! 0;

    Pop::Entities.view(Player, Body, Drawable).each: -> $entity, $player, $body, $draw {
        $player.on-ground = $body.is-solid: 0, 1;
        $player.on-ice    = $body.is-ice:   0, 1;

        # Player death
        if $body.check-spikes || $body.y > 128 {
            # sfx_timer=12 # TODO: Music
            # sfx(0)

            # deaths += 1 # TODO: Stats
            Pop::Entities.delete: $entity;
            $SHAKE = 10;

            for -π, * + π / 4 ...^ π -> $angle {
                Pop::Entities.create: DeadParticle.new(
                    xy    => $body.xy.add(4),
                    speed => Pop::Point.new( sin($angle) * 3, cos($angle) * 3 ),
                );
            }

            $RESTART-IN = 15;

            last;
        }

        # TODO: Smoke effects

        my $jump-pressed = Pop::Inputs.keyboard: 'c';
        my $jump = $jump-pressed && not $player.jump-pressed;
        $player.jump-pressed = $jump-pressed;

        my $dash-pressed = Pop::Inputs.keyboard: 'x';
        my $dash = $dash-pressed && not $player.dash-pressed;
        $player.dash-pressed = $dash-pressed;

        if $jump {
            $player.jump-buffer = 4;
        }
        elsif $player.jump-buffer > 0 {
            $player.jump-buffer--;
        }

        if $player.on-ground {
            $player.grace = 6; # Coyote time
            if $player.dash-jump < $player.max-dash-jump {
                # psfx(54) # TODO: Sound
                $player.dash-jump = $player.max-dash-jump;
            }
        }
        elsif $player.grace > 0 {
            $player.grace--;
        }

        $player.dash-effect-time--;
        if $player.dash-time > 0 {
            # init_object(smoke,this.x,this.y) # TODO: Smoke
            $player.dash-time--;

            $body.speed .= new:
                appr( $body.speed.x, $player.dash-target.x, $player.dash-accel.x ),
                appr( $body.speed.y, $player.dash-target.y, $player.dash-accel.y );
        }
        else {
            # Move
            my $acceleration
                =  $player.on-ground ?? 0.6
                !! $player.on-ice    ?? 0.05
                !! 0.4;

            my $dx = $body.speed.x.abs > $player.max-speed
                ?? appr( $body.speed.x, $body.speed.x.sign * $player.max-speed, $player.decceleration )
                !! appr( $body.speed.x, $input             * $player.max-speed, $acceleration );

            $body.speed .= new: $dx, $body.speed.y;

            # Facing
            $draw.flip-x = $dx < 0 if $dx;

            # Gravity
            my ( $max-fall, $gravity ) = 2, 0.21;
            $gravity *= 0.5 if $body.speed.y.abs <= 0.15;

            # Wall slide
            if $input && $body.is-solid( $input, 0 ) && !$body.is-ice: $input, 0 {
                $max-fall = 0.4;
                if 10.rand < 2 {
                    # init_object( smoke, this.x + input * 6, this.y ) # TODO: Smoke
                }
            }

            $body.speed .= new: $body.speed.x, appr $body.speed.y, $max-fall, $gravity
                unless $player.on-ground;

            # Jump
            if $player.jump-buffer > 0 {
                if $player.grace > 0 {
                    # Normal jump
                    $player.grace = $player.jump-buffer = 0;
                    $body.speed .= new: $body.speed.x, -2;
                    # init_object( smoke, this.x, this.y + 4 ) # TODO: Smoke
                }
                else {
                    # Wall jump
                    my $wall-dir
                        =  $body.is-solid( -3, 0 ) ?? -1
                        !! $body.is-solid(  3, 0 ) ??  1
                        !! 0;

                    if $wall-dir {
                        # psfx(2) # TODO: Sound
                        $player.jump-buffer = 0;
                        $body.speed .= new: -$wall-dir * ( $player.max-speed + 1 ), -2;
                        unless $body.is-ice: $wall-dir * 3, 0 {
                            # init_object( smoke, this.x + wall_dir * 6, this.y ) # TODO: Smoke
                        }
                    }
                }
            }

            # Dash
            if $dash {
                if $player.dash-jump > 0 {
                    # init_object(smoke,this.x,this.y) # TODO: Smoke

                    $player.dash-jump--;
                    $player.dash-time = 4;

                    # $has-dashed = True; # TODO: Flying fruits
                    $player.dash-effect-time = 10;

                    my $vertical-input
                        =  Pop::Inputs.keyboard('UP')   ?? -1
                        !! Pop::Inputs.keyboard('DOWN') ??  1
                        !! 0;

                    if $input {
                        if $vertical-input {
                            $body.speed .= new: $input * HALF-DASH, $vertical-input * HALF-DASH;
                        }
                        else {
                            $body.speed .= new: $input * FULL-DASH, 0;
                        }
                    }
                    elsif $vertical-input {
                        $body.speed .= new: 0, $vertical-input * FULL-DASH;
                    }
                    else {
                        $body.speed .= new: ( $draw.flip-x ?? -1 !! 1 ), 0;
                    }

                    # psfx(3) # TODO: Sound
                    # $freeze = 2;
                    $SHAKE = 6;

                    $player.dash-target .= new:
                        2 * $body.speed.x.sign,
                        2 * $body.speed.y.sign * ( $body.speed.y < 0 ?? 0.75 !! 1 );

                    $player.dash-accel .= new:
                        1.5 * ( $body.speed.y ?? DASH-FACTOR !! 1 ),
                        1.5 * ( $body.speed.x ?? DASH-FACTOR !! 1 );
                }
                else {
                    # psfx(9) # TODO: Sound
                    # init_object(smoke,this.x,this.y) # TODO: Smoke
                }
            }
        }

        # Animation
        $player.sprite-offset += 0.25;
        if not $player.on-ground {
            $draw.sprite = $body.is-solid( $input, 0 ) ?? 5 !! 3;
        }
        elsif Pop::Inputs.keyboard: 'DOWN' {
            $draw.sprite = 6;
        }
        elsif Pop::Inputs.keyboard: 'UP' {
            $draw.sprite = 7;
        }
        elsif $body.speed.x == 0 || $input == 0 {
            $draw.sprite = 1;
        }
        else {
            $draw.sprite = ( 1 + $player.sprite-offset % 4 ).Int;
        }

        # Next level
        next-room() if $body.y < -4 && level-index() < 30;

        my $player-hitbox = $body.hitbox.translate: $body.xy;

        Pop::Entities.view(Body, FallFloor).each: -> $_, $floor {
            # Nothing to do if floor is broken or breaking
            next unless $floor.state == 0;

            # If player is touching it, then we start breaking the floor
            if .hitbox.translate( .xy.sub: 1, 1 ).resize( 1, 1 ).collide: $player-hitbox {
                $floor.state = 1;
            }
        }

        Pop::Entities.view(Body, Drawable, Balloon).each: -> $body, $draw, $balloon {
            if $draw.sprite == 22 {
                $balloon.offset += 0.01;
                $body.xy .= new: $body.x, $balloon.start + sin( $balloon.offset) * 2;

                if $player.dash-jump < $player.max-dash-jump {
                    if $body.hitbox.translate( $body.xy ).collide: $player-hitbox {
                        # psfx(6); # TODO: Sound
                        # init_object(smoke,this.x,this.y) # TODO: Smoke

                        $player.dash-jump = $player.max-dash-jump;
                        $draw.sprite = 0;
                        $balloon.timer = 60;
                    }
                }
            }
            elsif $balloon.timer > 0 {
                $balloon.timer--;
            }
            else {
                # psfx(7) # TODO: Sound
                # init_object(smoke,this.x,this.y) # TODO: Smoke
                $draw.sprite = 22;
            }
        }

        Pop::Entities.view(Body, Platform).each: -> $body, $platform {
            given $body {
                when .x < -16 { .xy .= new: 128, .y }
                when .x > 128 { .xy .= new: -16, .y }
            }

            # Move the player body (the body in the outer scope)
            $::('OUTER::body').move-x: $body.x - $platform.last.round, 1
                if $body.hitbox.translate( $body.xy.add( 0, -1 ) ).collide: $player-hitbox;

            $platform.last = $body.x;
        }
    }

    Pop::Entities.view(Body, Drawable, Hair).each: -> $body, $draw, $hair {
        my $facing = $draw.flip-x ?? -1 !! 1;
        my $last = $body.xy.add:
            4 - $facing * 2,
            Pop::Inputs.keyboard('DOWN') ?? 4 !! 3;

        for $hair.balls {
            .xy .= add: ( $last.x - .x ) / 1.5, ( $last.y + 0.5 - .y ) / 1.5;
            $last = $_;
        }
    }

    Pop::Entities.view(Body, Drawable, Spring).each: -> $body, $draw, $spring {
        if $spring.hide-for > 0 {
            if --$spring.hide-for <= 0 {
                $draw.sprite = 18;
                $spring.delay = 0;
            }
        }
        elsif $draw.sprite == 18 {
            my ( $hit-body, $hit-player ) = Pop::Entities.get: $_, Body, Player, :!check
                with $body.collide: Player;

            if $hit-body && $hit-player && $hit-body.speed.y >= 0 {
                $draw.sprite = 19;
                $hit-body.xy .= new: $hit-body.x, $body.y - 4;
                $hit-body.speed .= new: $hit-body.speed.x * 0.2, -3;

                $hit-player.dash-jump = $hit-player.max-dash-jump;

                $spring.delay = 10;
                # init_object(smoke,this.x,this.y) # TODO: Smoke

                # If we trigger a spring on top of a fall-floor, we break it
                with $body.collide: FallFloor, 0, 1 {
                    .state = 1 with Pop::Entities.get: $_, FallFloor, :!check;
                }

                # psfx(8) TODO: Sound
            }
        }
        elsif $spring.delay && --$spring.delay <= 0 {
            $draw.sprite = 18;
        }

        # Begin hiding
        if $spring.hide-in && --$spring.hide-in <= 0 {
            $spring.hide-for = 60;
            $draw.sprite     = 0;
        }
    }

    Pop::Entities.view(Body, Drawable, FallFloor).each: -> $body, $draw, $floor {
        given $floor.state {
            when 1 { # Starting to break
                # psfx(15) # TODO: Sound

                $floor.state = 2;
                $floor.delay = 15; # How long until it falls

                # init_object(smoke,obj.x,obj.y) # TODO: Smoke

                # If we break a fall-floor, we hide any spring that is above
                with $body.collide: Spring, 0, -1 {
                    .hide-in = 15 with Pop::Entities.get: $_, Spring, :!check;
                }
            }
            when 2 { # Breaking
                if --$floor.delay <= 0 {
                    $floor.state = 3;
                    $floor.delay = 60; # How long it hides for

                    $draw.sprite = 0;

                    $body.collideable = False;
                }
                else {
                    $draw.sprite = 23 + ( 15 - $floor.delay ) div 5;
                }
            }
            when 3 { # Broken, waiting to reset
                if --$floor.delay <= 0 && !$body.check: Player {
                    # psfx(7) # TODO: Sound

                    $floor.state = 0;

                    $draw.sprite = 23;

                    $body.collideable = True;

                    # init_object(smoke,this.x,this.y) # TODO: Smoke
                }
            }
        }
    }
}

Pop.render: {
    cls;

    # screenshake
    if $SHAKE > 0 {
        camera();
        camera( -2 + (^5).pick, -2 + (^5).pick ) if --$SHAKE > 0;
    }

    unless room.is-title {
        Pop::Entities.view(Cloud).each: -> $_ {
            # TODO: Cloud color can be 14 in some cases. See 'new_bg'
            rectfill .xy, .xy.add( .size, 4 + ( 1 - .size / 64 ) * 12 ), 1;
        }
    }

    Pop::Graphics.draw: room.bg, ( 0, 0 );

    Pop::Entities.view(Hair).each: -> $_ { circfill .xy.round, .size, 8 for .balls }

    Pop::Entities.view(Body, Drawable, Balloon).each: -> $body, $draw, $balloon {
        next unless $draw.sprite;
        spr 13 + floor( ( $balloon.offset * 8 ) % 3 ), $body.xy.add( 0, 6 );
    }

    Pop::Entities.view(Body, Drawable).each: { $^draw.draw: $^body.xy }

    # For debug: draw hitboxes
    # Pop::Entities.view(Body).each: {
    #     my $box = .hitbox.translate: .xy;
    #     rect $box.xy, $box.wh.add( $box.xy ), 11;
    # }

    Pop::Entities.view(Body, Platform).each: -> $_, $ {
        spr 11, .xy.add( 0, -1 );
        spr 12, .xy.add( 8, -1 );
    }

    Pop::Entities.view(Particle).each: -> $_ {
        rectfill .xy, .xy.add( .size ), .color;
    }

    Pop::Entities.view(DeadParticle).each: -> $_ {
        rectfill .xy.sub( .t / 5 ), .xy.add( .t / 5 ), 14 + ( .t % 2 );
    }

    if room.is-title {
        print 'press button',  44,  96, 5;
        print 'maddy thorson', 42, 108, 5;
        print 'noel berry',    48, 114, 5;
    }
}

Pop.run :30fps;
