## NAME

Celeste - a Raku clone of the game by Noel Bery and Maddy Thorson

## DESCRIPTION

This is an attempt at recreating the classic [Pico-8] version of [Celeste],
by Noel Berry and Maddy Thorson, in [Raku] using [Pop]. This is part an
exploration of the original codebase, part a test of the capabilities of Pop
and Raku, and all around good fun.

[raku]: https://raku.org
[celeste]: https://www.lexaloffle.com/bbs/?pid=11722
[pico-8]: https://www.lexaloffle.com/pico-8.php
[pop]: https://gitlab.com/jjatria/pop

## CURRENT STATE

This is still relatively early in the process. Although the game is in a
playable state (and is just as much as fun as it should be), a full
playthrough is not yet possible because not all of the features needed to
complete all levels are implemented.

The current goal is to implement enough of the basic game logic to allow
a full playthrough to be possible. After that, work should start on
implementing most of the remaining visual effects, and later the sound
effects. Sound support is not yet implemnted in Pop itself, so this will
likely be what is implemented last.

To run the game in its current state, you'll need a copy of [Pop] in your
system. See that project for information on its current state and how to get
a copy. Once that is done, running the game from source is a matter of

    raku -Ipath/to/Pop bin/celeste-raku

## INSTALLATION

This is still a work in progress, so you probably do not want to install this
to run it. However, if that's what you're after, it should be a matter of
running

    zef install .

in the root of this repository, or

    zef install https://gitlab.com/jjatria/celeste-clone.git

anywhere else.

The game executable is in `bin/celeste-raku`, which should have been added to
the `PATH` upon installation as `celeste-raku`.

## AUTHOR

José Joaquín Atria <jjatria@gmail.com>

## ACKNOWLEDGEMENTS

All assets, as well as the original concept and implementation, are the
creation of Noel Berry and Maddy Thorson. This project is in large part a
tribute to their talents.

## COPYRIGHT AND LICENSE

Copyright 2021 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under
the Artistic License 2.0.
