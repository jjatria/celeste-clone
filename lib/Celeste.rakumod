unit class Celeste:ver<0.0.1>:auth<zef:jjatria>;

use Local::Console;
use Pop::Point :operators;
use Pop::Rect;
use Pop::Texture;
use Pop::Inputs;
use Pop::Entities;
use Pop::Graphics;

constant TILE-SIZE   is export = 8;
constant ROOM-SIZE   is export = 16;
constant SCREEN-SIZE is export = TILE-SIZE * ROOM-SIZE;
constant FULL-DASH   is export = 5;
constant DASH-FACTOR is export = 0.70710678118;
constant HALF-DASH   is export = FULL-DASH * DASH-FACTOR;


role Drawable is export {
    has Int  $.sprite is rw;
    has Bool $.flip-x is rw;
    has Bool $.flip-y is rw;

    method draw ( $xy --> Nil ) {
        spr $!sprite, $xy, $!flip-x, $!flip-y if $!sprite
    }
}

role Spawn is export {
    has Pop::Point $.target     .= zero;
    has            $.state is rw = 0;
    has Int        $.delay is rw;
}

role Player is export {
    has $.max-speed              = 1;
    has $.decceleration          = 0.15;
    has $.sprite-offset    is rw = 0;

    has $.on-ground        is rw = False;
    has $.on-ice           is rw = False;

    has $.jump-buffer      is rw = 0;
    has $.jump-pressed     is rw = False;
    has $.grace            is rw = 0;

    has $.dash-pressed     is rw = False;
    has $.max-dash-jump    is rw = 1;
    has $.dash-jump        is rw = $!max-dash-jump;
    has $.dash-time        is rw = 0;
    has $.dash-effect-time is rw = 0;

    has Pop::Point $.dash-target is rw .= zero;
    has Pop::Point $.dash-accel  is rw .= zero;
}

role Particle is export {
    has $.speed        = (^4 ).pick + 1;
    has $.size         = (^32).pick + 32;
    has $.offset is rw = 1.rand;
    has $.color        = (^1).pick + 6;

    has Pop::Point $.xy is rw handles < x y > .= new(
        (^128).rand, (^128).rand
    );
}

role DeadParticle is export {
    has            $.t     is rw  = 10;
    has Pop::Point $.xy    is rw .= zero;
    has Pop::Point $.speed       .= zero;
}

role Cloud does Particle is export {}

class Spring is export {
    has Int $.hide-in  is rw = 0;
    has Int $.hide-for is rw = 0;
    has Int $.delay    is rw = 0;
}

class Hair is export {
    my class Ball {
        has Pop::Point $.xy is rw handles < x y > .= zero;
        has            $.size;
    }

    has @.balls;

    method new ( :$xy ) {
        self.bless: balls => (^4).map: { Ball.new: :$xy, size => max 1, min 2, 3 - $_ }
    }
}

class FallFloor is export {
    has Int $.state is rw = 0;
    has Int $.delay is rw = 0;
}

class Balloon is export {
    has Int $.timer  is rw = 0;
    has Num $.offset is rw = 1.rand;
    has Int $.start  is required; # Should default to Body.y
}

class Platform is export {
    has Num $.last is required is rw;
}

role Body is export {
    has Pop::Rect  $.hitbox                               .= new: 8;
    has Pop::Point $.xy    is built is rw handles < x y > .= zero;
    has Pop::Point $.speed is built is rw                 .= zero;
    has Pop::Point $!remainder                            .= zero;
    has Bool       $.solid          is rw                  = True;
    has Bool       $.collideable    is rw                  = True;

    method update ( --> Nil ) {
        $!remainder .= add: $!speed;
        my ( $x, $y ) = |$!remainder.round;
        $!remainder .= sub: $x, $y;

        $.move-x($x) if $x;
        $.move-y($y) if $y;
    }

    method move-x ( $amount, $start = 0 --> Nil ) {
        if $!solid {
            my $step = $amount.sign;
            for $start .. $amount.abs {
                if not self.is-solid: $step, 0 {
                    $!xy .= add: $step, 0;
                }
                else {
                    $!remainder .= mul: 0, 1;
                    $!speed     .= mul: 0, 1;
                    last;
                }
            }
        }
        else {
            $!xy .= add: $amount, 0;
        }
    }

    method move-y ( $amount, $start = 0 --> Nil ) {
        if $!solid {
            my $step = $amount.sign;
            for $start .. $amount.abs {
                if not self.is-solid: 0, $step {
                    $!xy .= add: 0, $step;
                }
                else {
                    $!remainder .= mul: 1, 0;
                    $!speed     .= mul: 1, 0;
                    last;
                }
            }
        }
        else {
            $!xy .= add: 0, $amount;
        }
    }

    method check-spikes ( --> Bool ) {
        my ( $x, $y, $w, $h, $dx, $dy ) = |$!xy.add( $!hitbox.xy ), |$!hitbox.wh, |$!speed;
        my ( $right, $bottom ) = $x + $w - 1, $y + $h - 1;
        my ( $modx, $mody, $modw, $modh ) = ( $x, $y, $right, $bottom ).map: * %% 8;

        for max( 0, floor $x / 8 ) .. min( 15, floor $right / 8 ) -> $i {
            for max( 0, floor $y / 8 ) .. min( 15, floor $bottom / 8 ) -> $j {
                my $tile = tile-at $i, $j;

                return True if $tile == 17 && ( $modh >= 6 || $y + $h == $j * 8 + 8 ) && $dy >= 0;
                return True if $tile == 27 &&   $mody <= 2                            && $dy <= 0;
                return True if $tile == 43 &&   $modx <= 2                            && $dx <= 0;
                return True if $tile == 59 && ( $modw >= 6 || $x + $w == $i * 8 + 8 ) && $dx >= 0;
            }
        }

        return False;
    }

    method is-ice ( Int $dx, Int $dy --> Bool ) {
        ice-at |( $!xy + $!hitbox.xy + ( $dx, $dy ) ), |$!hitbox.wh
    }

    method is-solid ( Int $dx, Int $dy --> Bool ) {
        my $xy = $!xy + $!hitbox.xy + ( $dx, $dy );

        return True if $xy.x < 0 || $xy.x + $!hitbox.w > 128;

        return True if $dy > 0
            && !$.check( Platform, $dx,   0 )
            &&  $.check( Platform, $dx, $dy );

        solid-at( |$xy, |$!hitbox.wh )
            || $.check( FallFloor, $dx, $dy )
            # || self!check( FakeWall,  $dx, $dy );
    }

    method check ( |c --> Bool ) { so $.collide: |c }

    method collide ( $type where * !~~ Body, $x = 0, $y = 0 ) {
        my $hitbox = $!hitbox.translate: $!xy.add( $x, $y );

        Pop::Entities.view(Body, $type).each: -> $e, $_, $ {
            next if $_ === self || !.collideable;
            return $e if .hitbox.translate(.xy).collide: $hitbox;
        }

        Nil;
    }
}

class Room {
    has $.x = 0;
    has $.y = 0;
    has Pop::Texture $.bg;
    has Bool         $.is-title = False;
}

my Room $room;

# Utils

our sub appr ( $value, $target, $amount --> Num ) is export {
    $value > $target
        ?? max( $value - $amount, $target ).Num
        !! min( $value + $amount, $target ).Num
}

our sub solid-at ( |c --> Bool ) is export { tile-flag-at |c, 0 }
our sub   ice-at ( |c --> Bool ) is export { tile-flag-at |c, 4 }

our sub tile-flag-at ( Int() $x, Int() $y, Int() $w, Int() $h, Int() $flag --> Bool ) {
    for max( 0, $x div 8 ) .. min( 15, ( $x + $w - 1 ) div 8 ) -> $i {
        for max( 0, $y div 8 ) .. min( 15, ( $y + $h - 1 ) div 8 ) -> $j {
            return True if fget tile-at( $i, $j ), $flag;
        }
    }
    return False;
}

our sub tile-at ( Int $x, Int $y --> Int ) {
    mget( $room.x * 16 + $x, $room.y * 16 + $y )
}

our sub term:<room> is export { $room }

sub level-index ( --> Int ) is export { $room.x % 8 + $room.y * 8 }

sub next-room ( --> Nil ) is export {
    # TODO: Music
    # if $room.x == 2 && $room.y == 1 {
    #     music(30,500,7)
    # }
    # elsif $room.x == 3 && $room.y == 1 {
    #     music(20,500,7)
    # }
    # elsif $room.x == 4 && $room.y == 2 {
    #     music(30,500,7)
    # }
    # elsif $room.x == 5 && $room.y == 3 {
    #     music(30,500,7)
    # }

    if $room.x == 7 {
        load-room 0, $room.y + 1;
    }
    else {
        load-room $room.x + 1, $room.y;
    }
}

# Loads room data from the tile map and pre-renders it to a texture.
# Returns a hash (eventually an object?) with the level data, including
# the pre-rendered texture for the room.
sub load-room ( $x, $y --> Nil ) is export {
    # Clear entities, but recycle particles and clouds.
    my ( @clouds, @particles );
    Pop::Entities.view(Cloud   ).each: -> $c {    @clouds.push: $c }
    Pop::Entities.view(Particle).each: -> $c { @particles.push: $c }

    Pop::Entities.clear;

    for ^16 {
        Pop::Entities.create: @clouds[$_] // Cloud.new;
    }

    for ^24 {
        Pop::Entities.create: @particles[$_] // Particle.new(
            speed  => 0.25 + 5.rand,
            size   => ( 1, |( 0 xx 4 ) ).pick,
            offset => 1.rand,
            color  => (^1).pick + 6,
        )
    }

    my Pop::Texture $bg;

    given Pop::Graphics {
        .offset .= zero;

        unless $bg = Pop::Textures.get('level') {
            $bg = Pop::Textures.create: ( SCREEN-SIZE, SCREEN-SIZE ), 'level';
            $bg.blend-mode: 'blend';
        }

        .set-target: $bg;
        LEAVE .reset-target;

        .clear: 0, 0, 0, 0;
        for ^ROOM-SIZE -> $ty {
            for ^ROOM-SIZE -> $tx {
                my $tile = mget $x * ROOM-SIZE + $tx, $y * ROOM-SIZE + $ty;
                my $xy = Pop::Point.new: $tx * TILE-SIZE, $ty * TILE-SIZE;

                if $tile == 1 { # Player spawn
                    $tile = 0;
                    create-player $xy;
                }
                elsif $tile == 18 { # Spring
                    $tile = 0;
                    create-spring $xy;
                }
                elsif $tile == 22 { # Balloon
                    $tile = 0;
                    create-balloon $xy;
                }
                elsif $tile == 23 { # Fall floor
                    $tile = 0;
                    create-fall-floor $xy;
                }
                elsif $tile == 11 | 12 {
                    create-platform $xy, $tile == 11 ?? -1 !! 1;
                    $tile = 0;
                }

                spr $tile, $xy;
            }
        }
    }

    $room = Room.new: :$x, :$y, :$bg, is-title => $x == 7 && $y == 3;
}

sub create-player ( $target --> Nil ) {
    my $xy = Pop::Point.new: $target.x, Pop::Graphics.height;

    Pop::Entities.create:
        Drawable.new( sprite => 1 ),
        Spawn.new( :$target ),
        Hair.new( :$xy ),
        Body.new( :!solid, :$xy,
            speed  => Pop::Point.new( 0, -4 ),
            hitbox => Pop::Rect.new( 1, 3, 6, 5 ),
        ),
}

sub create-spring ( Pop::Point $xy is copy --> Nil ) {
    Pop::Entities.create:
        Spring.new,
        Drawable.new( sprite => 18 ),
        Body.new( :$xy, :!solid ),
}

sub create-balloon ( $xy --> Nil ) {
    Pop::Entities.create:
        Balloon.new( start => $xy.y.round ),
        Drawable.new( sprite => 22 ),
        Body.new( :$xy, hitbox => Pop::Rect.new( -1, -1, 10, 10 ) ),
}

sub create-fall-floor ( $xy --> Nil ) {
    Pop::Entities.create:
        FallFloor.new,
        Drawable.new( sprite => 23 ),
        Body.new( :$xy ),
}

sub create-platform ( $xy is copy, $dir --> Nil ) {
    $xy .= sub: 4, 0;

    Pop::Entities.create:
        Platform.new( last => $xy.x ),
        Body.new(
            :$xy, :!solid,
            hitbox => Pop::Rect.new( 16, 1 ),
            speed  => Pop::Point.new( $dir * 0.65, 0 ),
        ),
}
