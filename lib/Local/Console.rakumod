unit module Local::Console;

use Pop::Graphics;
use Pop::Point;
use Pop::Textures;

my constant TILE-SIZE   = 8;
my constant FONT-WIDTH  = 4;
my constant FONT-HEIGHT = 6;

my ( $MAP, @FLAGS, @SPRITES, %FONT );
my @COLORS = (
    # R     G     B
    [ 0x00, 0x00, 0x00 ], #  0 Black
    [ 0x1D, 0x2B, 0x53 ], #  1 Dark blue
    [ 0x7E, 0x25, 0x53 ], #  2 Dark purple
    [ 0x00, 0x87, 0x51 ], #  3 Dark green
    [ 0xAB, 0x52, 0x36 ], #  4 Brown
    [ 0x5F, 0x57, 0x4F ], #  5 Dark grey
    [ 0xC2, 0xC3, 0xC7 ], #  6 Light grey
    [ 0xFF, 0xF1, 0xE8 ], #  7 White
    [ 0xFF, 0x00, 0x4D ], #  8 Red
    [ 0xFF, 0xA3, 0x00 ], #  9 Orange
    [ 0xFF, 0xEC, 0x27 ], # 10 Yellow
    [ 0x00, 0xE4, 0x36 ], # 11 Green
    [ 0x29, 0xAD, 0xFF ], # 12 Blue
    [ 0x83, 0x76, 0x9C ], # 13 Lavender
    [ 0xFF, 0x77, 0xA8 ], # 14 Pink
    [ 0xFF, 0xCC, 0xAA ], # 15 Light peach
);

# Read tile map and sprite data like Pico-8 does
# TODO: Make this read a p8 file
our sub load ( IO() :$map, IO() :$sprites, IO() :$flags --> Nil ) is export {
    my $data = $map.IO.slurp.chomp.subst(/\s/, :g);

    my ( $i, $mid ) = 0, $data.chars div 2;
    $MAP = buf8.allocate: $mid;

    for $data.comb: 2 {
        $MAP[ $i / 2 ] = ( $i < $mid ?? $_ !! .flip ).parse-base: 16;
        $i += 2;
    }

    @SPRITES = gather with Pop::Textures.load: $sprites {
        for (^8).map( * * TILE-SIZE ) -> $y {
            for (^16).map( * * TILE-SIZE ) -> $x {
                take .make-sprite: :$x, :$y,
                    width => TILE-SIZE, height => TILE-SIZE;
            }
        }
    }

    @FLAGS = $flags.lines.split(/\s+/)».Int;

    %FONT = gather with Pop::Textures.load: %?RESOURCES<font.png> {
        for 'abcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()_+-=?:.'.comb.kv -> $i, $c {
            take $c => .make-sprite:
                x => $i * FONT-WIDTH, y => 0,
                w =>      FONT-WIDTH, h => FONT-HEIGHT;
        }
    }
}

# Read the tile ID for a given coordinate in the tile map
our sub mget ( Int $x, Int $y ) is export { $MAP[ $x + $y * 128 ] }

# Get the value of a flag of a sprite
our sub fget ( Int $tile, Int $flag --> Bool ) is export {
    $tile < @FLAGS.elems && so ( @FLAGS[$tile] +& ( 1 +< $flag ) )
}

our sub cls ( $color = 0 ) is export {
    Pop::Graphics.clear: |@COLORS[$color];
}

our sub spr (
    Int        $n,      # The sprite index to draw
    Pop::Point $xy,     # The x and y coordinates to draw the sprite at
    Bool       $flipx?, # Flip sprite horizontally
    Bool       $flipy?, # Flip sprite vertically
    --> Nil
) is export {
    Pop::Graphics.draw: @SPRITES[$n], $xy, :$flipx, :$flipy;
}

our sub rect ( $xy0, $xy1, $col --> Nil ) is export {
    Pop::Graphics.rectangle: $xy0, $xy1, @COLORS[$col];
}

our sub rectfill ( $xy0, $xy1, $col --> Nil ) is export {
    Pop::Graphics.rectangle: :fill, $xy0, $xy1, @COLORS[$col];
}

our sub circ ( $xy, $radius, $col --> Nil ) is export {
    Pop::Graphics.circle: $xy, $radius, @COLORS[$col];
}

our sub circfill ( $xy, $radius, $col --> Nil ) is export {
    Pop::Graphics.circle: :fill, $xy, $radius, @COLORS[$col];
}

our sub print ( Str $string, $x, $y, $col --> Nil ) is export {
    for $string.comb.kv -> $i, $c {
        next if $c eq ' ';
        my $font = %FONT{$c} or die "No font defined for '$c'!";

        $font.color-mod: |@COLORS[$col] unless $col == 7; # If not white

        Pop::Graphics.draw: $font, ( $x + $i * FONT-WIDTH, $y );

        $font.color-mod: 0xFF, 0xFF, 0xFF unless $col == 7;
    }
}

our proto sub camera (|c) is export {*}
our multi sub camera ( $x, $y --> Nil ) { Pop::Graphics.offset .= new: $x, $y }
our multi sub camera (        --> Nil ) { Pop::Graphics.offset .= zero }
